#!/usr/bin/env python3

import logging
import argparse
import json
from pathlib import Path
import rosbag
from klampt.math import se3,so3
import pandas as pd
import numpy
import math
import copy

from std_msgs.msg import Header
from geometry_msgs.msg import TransformStamped
from tf2_msgs.msg import TFMessage

import fiducials_for_metashape


def to_transform_stamped(se3, header, frame_id, parent_frame_id):

    camera_msg = TransformStamped()
    camera_msg.header = copy.deepcopy(header)
    camera_msg.header.seq = 0
    camera_msg.header.frame_id = parent_frame_id
    camera_msg.child_frame_id = frame_id
    camera_msg.transform.translation.x = se3[1][0]
    camera_msg.transform.translation.y = se3[1][1]
    camera_msg.transform.translation.z = se3[1][2]

    q = so3.quaternion(se3[0])

    # Klampt produces quaternion in w,x,y,z
    camera_msg.transform.rotation.w = q[0]
    camera_msg.transform.rotation.x = q[1]
    camera_msg.transform.rotation.y = q[2]
    camera_msg.transform.rotation.z = q[3]

    return camera_msg


def flatten(r):
    return [entry for row in r for entry in row]


if __name__=="__main__":

    parser = argparse.ArgumentParser(description="Interpolate a topic's position given a trajectory for another topic")
    parser.add_argument('bagfile', type=Path, help='Bagfile to process')
    parser.add_argument('srctopic', help='Source topic')
    parser.add_argument('dsttopic', help='Destination topic(s)')

    parser.add_argument('--output', '-o', required=True, type=Path, help="Output bagfile")

    parser.add_argument('--src-frame', default=None, help="Child frame id")
    parser.add_argument('--dst-frame', default=None, help="Child frame id")
    parser.add_argument('--parent-frame', default="base_link", help="Parent frame id")

    parser.add_argument("--trajectory", required=True, type=Path, help="Trajectory file")

    parser.add_argument("--lever-arm", type=Path, help="If specified, JSON file which gives from camera to sonar")

    parser.add_argument('-v', '--verbose', action='count', default=0)


    args = parser.parse_args()

    # logging setup
    levels = [logging.WARNING, logging.INFO, logging.DEBUG]
    level = levels[min(len(levels)-1,args.verbose)]  # capped to number of levels

    logging.basicConfig(level=level,
                        format='%(asctime)s - %(levelname)s - %(message)s')

    out = {}

    if not args.src_frame:
        args.src_frame = args.srctopic

    if not args.dst_frame:
        args.dst_frame = args.dsttopic

    lever = se3.identity()
    if args.lever_arm:

        with open(args.lever_arm) as fp:
            lever_json = json.load( fp )

        lever = ( flatten( numpy.transpose( lever_json["rotation"])), lever_json["translation"] )

    # Load trajectory file
    traj = fiducials_for_metashape.load_trajectory( args.trajectory )

    # reindex by sequence
    traj.set_index( 'seq', inplace=True )

    print(traj.head(5))
    print(traj.head(5).index )

    ts = []

    ## First pass, load srctopic timing into the trajectory
    with rosbag.Bag( args.bagfile, 'r' ) as inbag:

        for topic,msg,t in inbag.read_messages( topics=args.srctopic ):

            seq = int(msg.header.seq)

            if seq in traj.index:
                ts.append( [seq, msg.header.stamp.to_sec() ] )
            else:
                logging.warn("Couldn't find trajectory entry for sequence %d" % seq )


    timestamps = pd.DataFrame( ts, columns=['seq','timestamp'])
    print(timestamps.head(5))

    traj = traj.merge( timestamps, left_index=True, right_on='seq' )
    print(traj.head(5))

    with rosbag.Bag( args.output, 'w' ) as outbag:

        ## Second pass
        with rosbag.Bag( args.bagfile, 'r' ) as inbag:

            for topic,msg,t in inbag.read_messages():

                outbag.write(topic, msg, msg.header.stamp if msg._has_header else t)

                if topic in args.dsttopic:

                    ## Find surrounding trajectory points
                    t = msg.header.stamp.to_sec()

                    before = traj[ traj["timestamp"] < t ]
                    after = traj[ traj["timestamp"] >= t ]

                    if not (before.empty or after.empty):

                        before = before.iloc[ len(before)-1 ].to_dict()
                        after = after.iloc[0].to_dict()

                        u = (t - before["timestamp"]) / (after["timestamp"] - before["timestamp"])

                        #logging.debug("u = %.2f" % u)
                        interp = se3.interpolate( before["SE3"], after["SE3"], 0.0 )

                        lever_frame = se3.mul(interp,lever)

                        # print("Before: %s" % before)
                        # print("After: %s" % after)
                        #
                        # print("Interpolated:")
                        # print(interp)
                        #
                        # q = so3.quaternion(interp[0])
                        # print("Quaternion: %s" % q)

                        # rpy = so3.rpy( flat_r )
                        # rpy = [math.degrees(i) for i in rpy]
                        #
                        # logging.debug("R %.2f ;P %.2f ;Y %.2f" % (rpy[0], rpy[1], rpy[2]) )
                        # logging.debug("---------------------")

                        tf_msg = TFMessage()
                        tf_msg.transforms = []

                        tf_msg.transforms.append( to_transform_stamped( interp, msg.header, args.src_frame, args.parent_frame) )
                        tf_msg.transforms.append( to_transform_stamped( lever_frame, msg.header, args.dst_frame, args.parent_frame  ) )
                        tf_msg.transforms.append( to_transform_stamped( lever, msg.header, args.dst_frame, args.src_frame ) )
                        outbag.write( "/tf", tf_msg, msg.header.stamp )



    # with open(args.output, 'w') as fp:
    #     json.dump(out, fp, indent=2)
