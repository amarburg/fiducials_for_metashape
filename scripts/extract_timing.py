#!/usr/bin/env python3

import logging
import argparse
import json
from pathlib import Path
import rosbag

if __name__=="__main__":

    parser = argparse.ArgumentParser(description='Extract timing associated with topics in a bagfile')
    parser.add_argument('bagfile', type=Path, help='Bagfile to process')
    parser.add_argument('topics', nargs="*", help='Topics to extract timing')

    parser.add_argument('-v', '--verbose', action='count', default=0)

    parser.add_argument('--output', '-o', required=True, type=Path, help="Output file (JSON)")

    args = parser.parse_args()

    # logging setup
    levels = [logging.WARNING, logging.INFO, logging.DEBUG]
    level = levels[min(len(levels)-1,args.verbose)]  # capped to number of levels

    logging.basicConfig(level=level,
                        format='%(asctime)s - %(levelname)s - %(message)s')

    out = {}

    with rosbag.Bag( args.bagfile, 'r' ) as bag:

        for topic,msg,t in bag.read_messages():

            if topic in args.topics:

                if topic not in out:
                    out[topic] = {}

                out[topic][msg.header.seq] = msg.header.stamp.to_sec()

    with open(args.output, 'w') as fp:
        json.dump(out, fp, indent=2)
