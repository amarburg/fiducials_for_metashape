#!/usr/bin/env python3

import logging
import argparse
import json
from pathlib import Path
import numpy as np

import cv2 as cv


if __name__=="__main__":

    parser = argparse.ArgumentParser(description='Find Aruco tags and label control points in a sequence of images')
    parser.add_argument('inputs', nargs='*', type=Path, help='Images to extract')

    parser.add_argument('-v', '--verbose', action='count', default=0)
    parser.add_argument('--annotate-dir', type=Path, help='Draw annotated images to directory')

    parser.add_argument('--output', '-o', required=True, type=Path, help="Output file (JSON)")
    parser.add_argument('--outspec', required=True, type=Path, help="Output spec (YAML)")

    args = parser.parse_args()

    # logging setup
    levels = [logging.WARNING, logging.INFO, logging.DEBUG]
    level = levels[min(len(levels)-1,args.verbose)]  # capped to number of levels

    logging.basicConfig(level=level,
                        format='%(asctime)s - %(levelname)s - %(message)s')


#    dictionary = cv.aruco.Dictionary_get(cv.aruco.DICT_6X6_250)


    dictionary = cv.aruco.Dictionary_get(cv.aruco.DICT_APRILTAG_36h11)

    parameters =  cv.aruco.DetectorParameters_create()

    if args.output and not args.outspec:
        logging.error("If --output is specified, --outspec must be specified")
        exit()

    with open(args.outspec) as fp:
        outspec = json.load(fp)

    out = {}

    if args.annotate_dir:
        args.annotate_dir.mkdir(exist_ok=True)

    for image_file in args.inputs:

        logging.info("Processing %s" % image_file)

        image = cv.imread( str(image_file) )

        markerCorners, markerIds, rejectedCandidates = cv.aruco.detectMarkers(image, dictionary, parameters=parameters)

        if markerIds is not None and markerIds.any():

            logging.info("  found %d ids" % len(markerIds) )

            cv.aruco.drawDetectedMarkers( image, markerCorners, markerIds )

            imageinfo = {}

            for id,corners in zip(markerIds, markerCorners):

                id = id[0]
                corners = corners[0]

                if str(id) in outspec:
                    s = outspec[str(id)]

                    if 'corners' in s:
                        for c,corner in zip(s['corners'],corners):

                            if "name" in c:
                                imageinfo[ c["name"] ] = corner.tolist()

                    if 'center' in s:
                        center_spec = s['center']
                        if 'name' in center_spec:

                            center = np.mean(corners, axis=0)

                            imageinfo[ center_spec['name'] ] = center.tolist()

            out[ str(image_file.name) ] = imageinfo
        else:
            logging.info("  found no tags")


        if args.annotate_dir:
            outfile = args.annotate_dir / image_file.name
            logging.info("  writing annotated version to %s" % outfile)

            cv.imwrite( str(outfile), image )

    with open(args.output, 'w') as fp:
        json.dump(out, fp, indent=2)
