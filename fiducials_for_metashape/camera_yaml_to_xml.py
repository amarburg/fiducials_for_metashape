import yaml
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

import xml.etree.ElementTree as ET
import logging


#
#
def opencv_yaml_to_metashape(opencv_yaml):

    ## Essentially convert the OpenCV yaml format to the camera_info format

    camera_info = {
        'width': opencv_yaml['image_width'],
        'height': opencv_yaml['image_height'],
        'D': opencv_yaml['distortion_coefficients']['data'],
        'K': opencv_yaml['camera_matrix']['data'],
        'R': opencv_yaml['rectification_matrix']['data'],
        'P': opencv_yaml['projection_matrix']['data'],
        'distortion_model': opencv_yaml['camera_model']
    }

    return camera_info_to_metashape(camera_info)


# Assumes the input the dict resulting from loadin a camera_info yaml
# \return XML ElementTree in Metashape format
def camera_info_to_metashape(camera_info, use_rectification=False):

    if 'distortion_model' not in camera_info:
        return False

    if camera_info['distortion_model'] != 'plumb_bob':
        logging.error("Input model is not plumb_bob")
        return False


    # Here's an example XML file
    # <calibration>
    #   <projection>frame</projection>
    #   <width>512</width>
    #   <height>288</height>
    #   <f>303.82317599999999</f>
    #   <cx>-1.86127</cx>
    #   <cy>7.41657857614</cy>
    #   <b1>96.278400000000005</b1>
    #   <k1>0.62924400000000003</k1>
    #   <k2>-0.39455099999999999</k2>
    #   <p1>-0.0021785099999999998</p1>
    #   <p2>-0.0031373600000000001</p2>
    #   <date>2020-07-22T19:27:48Z</date>
    # </calibration>

    calib = ET.Element('calibration')
    proj = ET.SubElement(calib,'projection')
    proj.text = "frame"

    w = camera_info['width']
    width = ET.SubElement(calib,'width')
    width.text = str(w)

    h = camera_info['height']
    height = ET.SubElement(calib,'height')
    height.text = str(h)

    # Here are the conversions:
    #  camera_info     |     metashape
    #  d[0] aka k_1    | k_1
    #  d[1] aka k_2    | k_2
    #  d[2] aka t_1    | p_2   n.b. these two terms are flipped.  Opencv's p_1 == metashapes p_2
    #  d[3] aka t_2    | p_1
    #  d[4] aka k_3    | k_3
    #  f_y             | f
    #  f_x             | f + b_1
    #  alpha (skew)    | b_2
    #  c_x             | w/2 + c_x
    #  c_y             | h/2 + c_y


    k1 = ET.SubElement(calib,'k1')
    k2 = ET.SubElement(calib,'k2')
    p2 = ET.SubElement(calib,'p2')
    p1 = ET.SubElement(calib,'p1')
    k3 = ET.SubElement(calib,'k3')


    if use_rectification:
        fx = camera_info['P'][0]
        fy = camera_info['P'][5]
        cx = camera_info['P'][2]
        cy = camera_info['P'][6]
        alpha = camera_info['P'][1]

        ## Assume pre-undistorted imagery
        k1.text = "0"
        k2.text = "0"
        p2.text = "0"
        p1.text = "0"
        k3.text = "0"

    else:

        fx = camera_info['K'][0]
        fy = camera_info['K'][4]
        cx = camera_info['K'][2]
        cy = camera_info['K'][5]
        alpha = camera_info['K'][1]

        k1.text = str(camera_info['D'][0])
        k2.text = str(camera_info['D'][1])
        p2.text = str(camera_info['D'][2])
        p1.text = str(camera_info['D'][3])
        k3.text = str(camera_info['D'][4])



    f_elem = ET.SubElement(calib,'f')
    f_elem.text = str(fy)

    b1_elem = ET.SubElement(calib,'b1')
    b1_elem.text = str( fx-fy )

    cx_elem = ET.SubElement(calib,'cx')
    cx_elem.text = str( cx-w/2 )

    cy_elem = ET.SubElement(calib,'cy')
    cy_elem.text = str( cy-h/2 )

    b2_elem = ET.SubElement(calib,'b2')
    b2_elem.text = str( alpha )

    return calib
