#!/usr/bin/env python3


import logging
import Metashape
import json
import argparse
from pathlib import Path

parser = argparse.ArgumentParser(description='Find Aruco tags and label control points')
parser.add_argument('json', nargs="?", type=Path, help='JSON output from extract_arusco')
parser.add_argument('psx', nargs="?", type=Path, help='Metashape project to modify')

parser.add_argument('--chunk', help='Chunk to modify')
parser.add_argument('-v', '--verbose', action='count', default=0)

args = parser.parse_args()

levels = [logging.WARNING, logging.INFO, logging.DEBUG]
level = levels[min(len(levels)-1,args.verbose)]  # capped to number of levels

logging.basicConfig(level=level,
                    format='%(asctime)s - %(levelname)s - %(message)s')


with open(str(args.json)) as fp:
    indata = json.load(fp)

doc = Metashape.app.document
doc.open( str(args.psx), read_only=False )

if args.chunk:

    for chunk in doc.chunks:

        logging.info("Checking chunk %s" % chunk.label)

        if chunk.label == args.chunk:
            logging.info("Found chunk %s" % args.chunk)
            break

else:
    # Otherwise use first chunk
    chunk = doc.chunk
    logging.info("Processing first chunk %s" % chunk.label)

if not chunk:
    logging.fatal("Unable to find chunk %s" % args.chunk)
    exit(-1)


for imagefile,points in indata.items():

    ## Strip off extension
    imagefile = Path(imagefile).stem

    for camera in chunk.cameras:

        #logging.info("Compare %s to %s" % (camera.label, imagefile))
        if camera.label == imagefile:
            logging.info("Found match for \"%s\"" % camera.label)

            for marker_name,location in points.items():

                flag=0
                for marker in chunk.markers:	#searching for the marker (comparing with all the marker labels in chunk)

                    if marker.label == marker_name:
                      marker.projections[camera] =  Metashape.Marker.Projection(Metashape.Vector(location), True)		#setting up marker projection of the correct photo)
                      flag = 1
                      break

                if not flag:
                    marker = chunk.addMarker()
                    marker.label = marker_name
                    marker.projections[camera] =  Metashape.Marker.Projection(Metashape.Vector(location), True)
                    break



## Assumes reference points have been defined in project?
chunk.updateTransform()

doc.save()
