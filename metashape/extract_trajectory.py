#!/usr/bin/env python3

import math
import logging
import Metashape
import json
import argparse
from pathlib import Path


parser = argparse.ArgumentParser(description='Extract trajectory for all cameras')
parser.add_argument('psx', nargs="?", type=Path, help='Metashape project to modify')
parser.add_argument('--chunk', help='Chunk to modify')
parser.add_argument('--output', '-o', required=True, type=Path, help="JSON Output destination")

parser.add_argument('-v', '--verbose', action='count', default=0)

args = parser.parse_args()

levels = [logging.WARNING, logging.INFO, logging.DEBUG]
level = levels[min(len(levels)-1,args.verbose)]  # capped to number of levels

logging.basicConfig(level=level,
                    format='%(asctime)s - %(levelname)s - %(message)s')

doc = Metashape.app.document
doc.open( str(args.psx) )

if args.chunk:

    for chunk in doc.chunks:

        logging.info("Checking chunk %s" % chunk.label)

        if chunk.label == args.chunk:
            logging.info("Found chunk %s" % args.chunk)
            break

else:
    # Otherwise use first chunk
    chunk = doc.chunk
    logging.info("Processing first chunk %s" % chunk.label)

if not chunk:
    logging.fatal("Unable to find chunk %s" % args.chunk)
    exit(-1)


## Extract chunk transform
chunk_tx = chunk.transform
print(chunk_tx)

json_output = []

def Vector_to_list( vector ):
    sz = vector.size

    line = []
    for j in range( sz ):
        line.append( vector[j] )

    return line

def Matrix_to_list( matrix ):
    sz = matrix.size
    out = []
    for i in range( sz[0] ):
        line = []
        for j in range( sz[1] ):
            line.append( matrix.row(i)[j] )

        out.append(line)

    return out

for camera in chunk.cameras:

    camera_tx = camera.transform

    if camera_tx:
        # Transform camera to global coords

        global_tx = chunk_tx.matrix * camera_tx * Metashape.Matrix().Diag([1, -1, -1, 1])

        estimated_coord = chunk.crs.project(chunk.transform.matrix.mulp(camera.center))
        T = chunk.transform.matrix
        m = chunk.crs.localframe(T.mulp(camera.center)) #transformation matrix to the LSE coordinates in the given point
        R = (m * T * camera.transform * Metashape.Matrix().Diag([1, -1, -1, 1])).rotation()
        #
        # logging.info(global_tx)
        # logging.info(global_tx.rotation())
        # logging.info(global_tx.translation())
        # logging.info(global_tx.scale())

        ypr = Metashape.utils.mat2ypr(R)  ## In degrees

        json_output.append( { 'key': camera.key,
                                'label': camera.label,
                                'rotation': Matrix_to_list(R),
                                'translation': Vector_to_list(estimated_coord),
                                'scale': global_tx.scale(),
                                'matrix': Matrix_to_list(global_tx),
                                'ypr': Vector_to_list(ypr) })

with open(str(args.output),'w') as fp:
    json.dump( json_output, fp, indent=2)
