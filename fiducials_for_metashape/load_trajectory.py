#!/usr/bin/env python

import pandas as pd
import json

import re
from klampt.math import se3
import numpy

def flatten(r):
    return [entry for row in r for entry in row]

def load_trajectory( filename ):

    with open(filename) as fp:
        trajectory = json.load(fp)

    def find_sequence_number(label):

        m = re.search(r'(\d+)', label)
        if m:
            return int( m.group(0) )

        return 0

    flat_traj = {}
    flat_traj["label"] = [ t["label"] for t in trajectory]
    flat_traj["seq"] = [ find_sequence_number( t["label"]) for t in trajectory]
    flat_traj["X"] = [ t["translation"][0] for t in trajectory]
    flat_traj["Y"] = [ t["translation"][1] for t in trajectory]
    flat_traj["Z"] = [ t["translation"][2] for t in trajectory]
    flat_traj["R"] = [ t["rotation"] for t in trajectory]
    flat_traj["scale"] = [ float(t["scale"]) for t in trajectory ]

    # klampt's SE32 representation is 2-tuple consisting of a 9-tuple and a 3-tuple
    # Just remember the 9-tuple is column-major
    # but from_homogeneous expects a list-of-lists 4x4 matrix

    flat_traj["SE3"] = [ (flatten(numpy.transpose( t["rotation"] )), t["translation"]) for t in trajectory]

    # se3.from_homogeneous does not normalize the rotation matrix
    #flat_traj["SE3"] = [ se3.from_homogeneous( t["matrix"] ) for t in trajectory]

    df = pd.DataFrame( flat_traj, columns=['label','seq', 'X','Y','Z','R','SE3', "scale"])

    return df
