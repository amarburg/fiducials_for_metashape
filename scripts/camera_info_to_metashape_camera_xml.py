#!/usr/bin/env python3

import logging
import argparse
from pathlib import Path
import yaml
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

import fiducials_for_metashape as FM
import xml.etree.ElementTree as ET

if __name__=="__main__":

    parser = argparse.ArgumentParser(description='Converts ROS camera_info to Photoscan Camera XML format')
    parser.add_argument('input', nargs=1, type=Path, help='YAML format camera_info data')
    parser.add_argument('output', nargs=1, type=Path, help='Output Metashape XML')

    parser.add_argument('-v', '--verbose', action='count', default=0)

    args = parser.parse_args()

    # logging setup
    levels = [logging.WARNING, logging.INFO, logging.DEBUG]
    level = levels[min(len(levels)-1,args.verbose)]  # capped to number of levels

    logging.basicConfig(level=level,
                        format='%(asctime)s - %(levelname)s - %(message)s')

    with open(args.input[0]) as fp:
        camera_info = yaml.load( fp, Loader=Loader )


    logging.debug("Camera info: %s" % camera_info)


    if 'K' in camera_info:
        calib = FM.camera_info_to_metashape( camera_info )
    elif 'camera_matrix' in camera_info:
        calib = FM.opencv_yaml_to_metashape( camera_info )

    with open(args.output[0], 'wb') as fp:
        xml_str = ET.tostring(calib)
        fp.write(xml_str)
