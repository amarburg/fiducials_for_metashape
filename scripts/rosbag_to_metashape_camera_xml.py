#!/usr/bin/env python3

import logging
import rosbag
import argparse
from pathlib import Path
import yaml
import sys

try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

import fiducials_for_metashape as FM

import xml.etree.ElementTree as ET


if __name__=="__main__":

    parser = argparse.ArgumentParser(description='Reads a rosbag, extracts a camera_info message, creates Metashape camera XML')

    parser.add_argument('bagfile', type=Path, help='rosbag to read')
    parser.add_argument('--topic', '-t', help='camera_info topic; otherwise will attempt to autodetect')
    parser.add_argument('xmlfile', type=Path, help='Output XML file')

    parser.add_argument('-v', '--verbose', action='count', default=0)
    parser.add_argument("--save-yaml", type=Path, help="If set, the YAML camera_info is stored")

    parser.add_argument("--rectification", action='store_true', help="Use the rectification projection matrix P rather than the K matrix")

    args = parser.parse_args()

    # logging setup
    levels = [logging.WARNING, logging.INFO, logging.DEBUG]
    level = levels[min(len(levels)-1,args.verbose)]  # capped to number of levels

    logging.basicConfig(level=level,
                        format='%(asctime)s - %(levelname)s - %(message)s')

    camera_info = None

    with rosbag.Bag( args.bagfile, 'r' ) as bag:

        use_this = False

        for topic,msg,t in bag.read_messages():


            if args.topic is not None:

                logging.debug("Comparing bag topic %s to %s" % (topic, args.topic))
                if topic == args.topic:
                    logging.debug(msg)
                    use_this = True

            else:

                if re.search( r'camera_info', topic ):
                    logging.warning("Using camera info from topic %s" % topic )
                    use_this = True


            if use_this:
                camera_info = yaml.safe_load( str(msg) )

                if args.save_yaml:
                    with open(args.save_yaml,'w') as fp:
                        fp.write(str(msg))

                break



    if not camera_info:
        logging.error("Could not find camera_info in bagfile")
        sys.exit(-1)


    calib = FM.camera_info_to_metashape( camera_info, use_rectification=args.rectification )

    with open(args.xmlfile, 'wb') as fp:
        xml_str = ET.tostring(calib)
        fp.write(xml_str)
